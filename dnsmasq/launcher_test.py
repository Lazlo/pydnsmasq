import unittest
from launcher import Launcher

fake_get_processes_retval = []

def fake_get_processes(proc_name):
	global fake_get_processes_retval
	return fake_get_processes_retval

class LauncherTestCase(unittest.TestCase):

	def setUp(self):
		self.c = Launcher
		self.obj = self.c()
		# Save real 'get_processes'
		self.real_get_processes = self.c.get_processes
		# Install fake 'get_processes'
		self.obj.get_processes = fake_get_processes

	def test_is_running_returns_false_when_no_dnsmasq_process_exists(self):
		global fake_get_processes_retval
		fake_get_processes_retval = []
		self.assertEqual(False, self.obj.is_running('not-existing-process-name'))

	def test_is_running_returns_true_when_dnsmasq_process_exists(self):
		global fake_get_processes_retval
		fake_get_processes_retval = [42]
		self.assertEqual(True, self.obj.is_running('foobar'))
