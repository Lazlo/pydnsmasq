class Launcher(object):

	def get_processes(self, proc_name):
		import psutil
		running = [p.info for p in psutil.process_iter(attrs=['pid', 'name']) if proc_name in p.info['name']]
		return running

	def is_running(self, proc_name='dnsmasq'):
		running = self.get_processes(proc_name)
		if len(running) > 0:
			return True
		return False
