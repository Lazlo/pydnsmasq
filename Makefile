default: all
all: test

TEST_CMD=python -m unittest discover -p "*_test.py"

test:
	$(TEST_CMD)
vtest:
	$(TEST_CMD) -v
